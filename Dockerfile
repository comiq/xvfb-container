FROM alpine:latest

RUN apk update
RUN apk add --no-cache \
  xvfb \
  dbus \
  ttf-freefont \
  python \
  py-pip \
  py-psutil \
  python-dev \
  gcc

RUN pip install \
  robotframework==3.0.2 \
  robotremoteserver

ENV DISPLAY=0
ENV SCREEN=0
ENV DISPLAY_MODE=1920x1080x24
ENV LIBRARY_PORT=8270


EXPOSE 6000
EXPOSE 8270

COPY xvfb.sh /usr/local/bin/
COPY XvfbControl.py XvfbControl.py
RUN chmod ug+x /usr/local/bin/xvfb.sh && \
  chmod ug+x XvfbControl.py
ENTRYPOINT ["/usr/local/bin/xvfb.sh"]
