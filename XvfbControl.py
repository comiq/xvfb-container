from robot.api import logger
from robot.libraries.Process import Process

import time
import os

def remote_log_message(message, level, html=False):
    print '{} {}'.format(level, message)

logger.write = remote_log_message

class XvfbControlLibrary(object):

  def __init__(self):
    self._processlib = Process()
    self.xvfb = None

  def open_display(self):
    if self.xvfb and self._processlib.is_process_running(self.xvfb):
      logger.write('{0} Xvfb is already running'.format(time.time() * 1000), "INFO")
    else:
      logger.write('{0} Starting Up Xvfb'.format(time.time() * 1000), "INFO")
      self.xvfb = self._processlib.start_process(
        "/usr/bin/Xvfb :{0} -ac -listen tcp -screen {1} {2}".format(
          os.environ["DISPLAY"],
          os.environ["SCREEN"],
          os.environ["DISPLAY_MODE"]),
         shell=True)

  def close_display(self):
    if self.xvfb and self._processlib.is_process_running(self.xvfb):
      logger.write("Killing Xvfb", "INFO")
      self._processlib.terminate_process(self.xvfb)
      self.xvfb = None
    else:
      logger.write('{0} Xvfv is not running'.format(time.time() * 1000), "INFO")

  def is_display_open(self):
    logger.write('{0} Checking is Xvfb running'.format(time.time() * 1000), "INFO")
    return self.xvfb and self._processlib.is_process_running(self.xvfb)

if __name__ == '__main__':
  import sys
  from robotremoteserver import RobotRemoteServer

  RobotRemoteServer(XvfbControlLibrary(), host="0.0.0.0", port=int(os.environ["LIBRARY_PORT"]), allow_stop=False)
