#!/bin/sh

if [ -f /tmp/.X0-lock ]; then
  rm -f /tmp/.X0-lock
fi

case "$1" in
  remote)
    echo "Starting as Robotframework Remote Library"
    exec python XvfbControl.py
    ;;
  *)
    echo "Starting as Standalone"
    exec Xvfb :$DISPLAY -ac -listen tcp -screen $SCREEN $DISPLAY_MODE
    ;;
esac
